package com.supermap.ar;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.eqgis.eqtool.tmp.Utils;
import com.supermap.ar.path.Destination;
import com.supermap.ar.path.NaviPath;
import com.supermap.data.Environment;
import com.supermap.data.Workspace;
import com.supermap.data.WorkspaceConnectionInfo;
import com.supermap.data.WorkspaceType;
import com.unity3d.player.UnityPlayerActivity;

/**
 * @author tanyx 2022/3/24
 * @version 1.0
 * <br/>SampleCode:<br/>
 * <code>
 *
 * </code>
 **/
public class MainActivity extends UnityPlayerActivity {

    private static final String TAG = "IKKYU";
    private static NaviPath mNaviPath;
    private static String sdCard = android.os.Environment.getExternalStorageDirectory().getAbsolutePath().toString();
    private static Workspace workspace;
//    @Override
//    public void onCreateForegroundView(Bundle bundle) {
////        setForegroundView(R.layout.activity_foreground);
////        efView = getForegroundView().findViewById(R.id.ef_view);
//    }
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        Utils.requestPermissions(this);
//        super.onCreate(savedInstanceState);
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.e(TAG, "onCreate: ");
//        //载入Unity场景
        super.onCreate(savedInstanceState);
        Utils.requestPermissions(this);
//        Toast.makeText(mActivity, "init", Toast.LENGTH_SHORT).show();
        Environment.setLicensePath(sdCard + "/SuperMap/license/");
        Environment.initialization(this);

        WorkspaceConnectionInfo info = new WorkspaceConnectionInfo();
        info.setType(WorkspaceType.SMWU);
        info.setServer(sdCard + "/SuperMap/arglasses/arglasses.smwu");
        workspace = new Workspace();
        boolean open = workspace.open(info);
        if (!open){
            Toast.makeText(this, "工作空间打开失败，\n请检查数据是否存在！", Toast.LENGTH_SHORT).show();
        }

        Destination.getInstance().init(workspace);
    }
}
