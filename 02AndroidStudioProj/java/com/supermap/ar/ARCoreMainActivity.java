package com.supermap.ar;

import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.eqgis.eqtool.tmp.Utils;
import com.google.are.sceneform.Camera;
import com.google.are.sceneform.math.Quaternion;
import com.google.are.sceneform.math.Vector3;
import com.supermap.ar.areffect.AREffectElement;
import com.supermap.ar.areffect.AREffectView;
import com.supermap.ar.areffect.ARViewElement;
import com.supermap.ar.areffect.TouchResult;
import com.supermap.ar.areffect.Vector;
import com.supermap.ar.areffect.preset.BaseShape;
import com.supermap.ar.areffect.preset.Shape;
import com.supermap.ar.path.Destination;
import com.supermap.ar.path.NaviPath;
import com.supermap.ar.unity.UnityToolkit;
import com.supermap.ar.unity.player.ForeUnityActivity;
import com.supermap.ar.unity.slam.CameraPoseData;
import com.supermap.ar.unity.slam.SlamObject;
import com.supermap.ar.unity.utils.CoordinateUtils;
import com.supermap.data.Environment;
import com.supermap.data.Workspace;
import com.supermap.data.WorkspaceConnectionInfo;
import com.supermap.data.WorkspaceType;
import com.unity3d.player.R;

/**
 * @author tanyx 2022/3/4
 * @version 1.0
 * <pre>
 *     当前sceneform依赖com.eqgis:sceneform-sm:1.19.10
 * </pre>
 * <br/>SampleCode:<br/>
 * <code>
 *
 * </code>
 **/
public class ARCoreMainActivity extends ForeUnityActivity {
    private static final String TAG = "IKKYU";
    private static NaviPath mNaviPath;
    private static String sdCard = android.os.Environment.getExternalStorageDirectory().getAbsolutePath().toString();
    private static Workspace workspace;
//    @Override
//    protected void onCreateBackgroundView(Bundle savedInstanceState) {
//        Log.i("IKKYU_MainActivity", "onCreateBackgroundView: ");
//        setBackgroundView(R.layout.activity_main);
//    }

    public AREffectView efView;
    private Camera camera;
    private AREffectElement arEffectElement;

    private Point3D destination;

    @Override
    public void onStartSlam() {
        camera = efView.getSceneView().getScene().getCamera();
    }

    @Override
    public void onDestroySlam() {
        efView.onDestroy();
    }

    /**
     * 更新相机姿态
     * @param cameraPoseData
     */
    @Override
    public void updateCameraPose(CameraPoseData cameraPoseData) {
        SlamObject.getInstance().updateUnityCamera(efView);
//        Vector3 po = CoordinateUtils.convert(camera.getWorldPosition());
//        Quaternion ra = CoordinateUtils.convert(camera.getWorldRotation());
//        float fov = camera.getVerticalFovDegrees();
//        float nearClipPlane = camera.getNearClipPlane();
//        float farClipPlane = camera.getFarClipPlane();
//
//        cameraPoseData.setAspect((float) efView.getWidth() /(float)efView.getHeight());
//        cameraPoseData.setFar(farClipPlane);
//        cameraPoseData.setNear(nearClipPlane);
//        cameraPoseData.setFov(fov);
//        cameraPoseData.setPositionX(po.x);
//        cameraPoseData.setPositionY(po.y);
//        cameraPoseData.setPositionZ(po.z);
//
//        cameraPoseData.setRotationX(ra.x);
//        cameraPoseData.setRotationY(ra.y);
//        cameraPoseData.setRotationZ(ra.z);
//        cameraPoseData.setRotationW(ra.w);

        if (arEffectElement!=null){
            arEffectElement.setPosition(camera.getWorldPosition());
//            arEffectElement.setRotationAngleNoRepeat(new Quaternion(Vector3.up(),ra.y));
        }
    }

    @Override
    public void onCreateBackgroundView(Bundle savedInstanceState) {
        setBackgroundView(R.layout.activity_main2);
        efView = getBackgroundView().findViewById(R.id.ef_view);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.e(TAG, "onCreate: ");
//        //载入Unity场景
        super.onCreate(savedInstanceState);
        Utils.requestPermissions(this);
//        Toast.makeText(mActivity, "init", Toast.LENGTH_SHORT).show();
        Environment.setLicensePath(sdCard + "/SuperMap/license/");
        Environment.initialization(this);

        WorkspaceConnectionInfo info = new WorkspaceConnectionInfo();
        info.setType(WorkspaceType.SMWU);
        info.setServer(sdCard + "/SuperMap/arglasses/arglasses.smwu");
        workspace = new Workspace();
        boolean open = workspace.open(info);
        if (!open){
            Toast.makeText(this, "工作空间打开失败，\n请检查数据是否存在！", Toast.LENGTH_SHORT).show();
        }

        Destination.getInstance().init(workspace);


        arEffectElement = new AREffectElement(this);
        arEffectElement.setParentNode(efView);

        ARViewElement arViewElement = new ARViewElement(this);
        arViewElement.setParentNode(arEffectElement);
        arViewElement.setRelativePosition(new Point3D(0,1,-2));
        arViewElement.getAnchorNode().setLocalRotation(new Quaternion(Vector3.right(),-60));
        arViewElement.loadModel(R.layout.main_ui);


    }

    private void createPath() {
        Point3D c = efView.getCameraPosition();
        String s = Destination.getInstance().doPathAnalyst(c.x, c.y, destination.x, destination.y);

        UnityToolkit.callUnity("RoadPathLoader","AddPath",s);
    }

    public void clickTYQ(View e){
        UnityToolkit.callUnity("RoadPathLoader","ClearPath","");
    }
    public void clickWH(View e){
        destination = new Point3D(1.6f,13.6F,0);
        createPath();
    }
    public void clickJG(View e){
        destination = new Point3D(-16.8F,15.6F,0);
        createPath();
    }


    public void clickGK(View e){
        destination = new Point3D(-12F,10.4F,0);
        createPath();
    }
    public void clickLC(View e){
        destination = new Point3D(-5.6f,3.2F,0);
        createPath();
    }
    public void clickFY(View e){
        destination = new Point3D(-7.2f,9.6F,0);
        createPath();
    }
    public void clickSM(View e){
        destination = new Point3D(-8f,5.6F,0);
        createPath();
    }
    public void clickQQYH(View e){
        destination = new Point3D(-7.2f,15.6F,0);
        createPath();
    }
    public void clickCPTX(View e){
        destination = new Point3D(-14.4f,15.6F,0);
        createPath();
    }



}
