package com.supermap.ar.path;

import android.util.Log;

import com.supermap.analyst.networkanalyst.TransportationAnalyst;
import com.supermap.analyst.networkanalyst.TransportationAnalystParameter;
import com.supermap.analyst.networkanalyst.TransportationAnalystResult;
import com.supermap.analyst.networkanalyst.TransportationAnalystSetting;
import com.supermap.analyst.networkanalyst.WeightFieldInfo;
import com.supermap.analyst.networkanalyst.WeightFieldInfos;
import com.supermap.data.DatasetVector;
import com.supermap.data.Datasource;
import com.supermap.data.Point2Ds;

/**
 * 路径分析工具
 * <p>
 *     需要路网数据集，需要指定途经点，
 *     路网数据集采用默认设置
 * </p>
 * @author IKKYU 2022年3月26日14:17:37
 */
public class PathAnalystHelper {
    private static String m_nodeID = "SmNodeID";
    private static String m_edgeID = "SmEdgeID";

    private static final String TAG = "MapData";
    private Datasource datasource;

    public PathAnalystHelper(Datasource datasource) {
        this.datasource = datasource;
    }

    private TransportationAnalyst m_analyst = null;

    /**
     * 获取路径
     * @param netDataName
     * @param m_Points 第一个点为起点，最后一个点为终点
     * @return
     */
    public TransportationAnalystResult getPath(String netDataName, Point2Ds m_Points){
        if (m_analyst == null){
            DatasetVector datasetLine = (DatasetVector) datasource.getDatasets().get(netDataName);

            // 设置网络分析基本环境，这一步骤需要设置　分析权重、节点、弧段标识字段、容限
            TransportationAnalystSetting setting = new TransportationAnalystSetting();
            setting.setNetworkDataset(datasetLine);
            setting.setEdgeIDField(m_edgeID);
            setting.setNodeIDField(m_nodeID);
//        setting.setEdgeNameField("roadName");

            setting.setTolerance(2);

            WeightFieldInfos weightFieldInfos = new WeightFieldInfos();
            WeightFieldInfo weightFieldInfo = new WeightFieldInfo();
            weightFieldInfo.setFTWeightField("smLength");
            weightFieldInfo.setTFWeightField("smLength");
            weightFieldInfo.setName("length");
            weightFieldInfos.add(weightFieldInfo);
            setting.setWeightFieldInfos(weightFieldInfos);
            setting.setFNodeIDField("SmFNode");
            setting.setTNodeIDField("SmTNode");

            //构造交通网络分析对象，加载环境设置对象
            m_analyst = new TransportationAnalyst();
            m_analyst.setAnalystSetting(setting);
            m_analyst.load();
        }

        //分析路径
        TransportationAnalystParameter parameter = new TransportationAnalystParameter();
        parameter.setWeightName("length");

        /**
         * 必须将交通网络分析参数设置（TransportationAnalystParameter）对象的 isPathGuidesReturn 方法设置为 true，才能从分析结果中获取到行驶导引集合。
         * */
        //设置最佳路径分析的返回对象
        parameter.setPoints(m_Points);
        parameter.setNodesReturn(true);
        parameter.setEdgesReturn(true);
        parameter.setPathGuidesReturn(true);//true
        parameter.setRoutesReturn(true);
        TransportationAnalystResult path;
        try {
            path = m_analyst.findPath(parameter, false);

        }catch (Exception e){
            Log.e("MapData", "getPath: ",e);
            path = null;
        }

        return path;
    }

}
