package com.supermap.ar.path;

import com.google.are.sceneform.math.Vector3;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * 导航路径
 */
public class NaviPath {

    @SerializedName("PathPoints")
    private ArrayList<Vector3> pathPoints = new ArrayList<>();

    public ArrayList<Vector3> getPathPoints() {
        return pathPoints;
    }

    public void setPathPoints(ArrayList<Vector3> pathPoints) {
        this.pathPoints = pathPoints;
    }
}
