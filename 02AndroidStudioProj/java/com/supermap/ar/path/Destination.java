package com.supermap.ar.path;

import android.util.Log;

import androidx.annotation.Nullable;

import com.google.are.sceneform.math.Vector3;
import com.google.gson.Gson;
import com.supermap.analyst.networkanalyst.TransportationAnalystResult;
import com.supermap.ar.Point3D;
import com.supermap.ar.areffect.PointConvertTool;
import com.supermap.ar.areffect.preset.PresetUtils;
import com.supermap.ar.unity.utils.CoordinateUtils;
import com.supermap.data.GeoLineM;
import com.supermap.data.Point;
import com.supermap.data.Point2D;
import com.supermap.data.Point2Ds;
import com.supermap.data.Workspace;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author tanyx 2022/3/26
 * @version 1.0
 * <br/>SampleCode:<br/>
 * <code>
 *
 * </code>
 * <pre>
 *     与Unity中Destination.cs结合使用
 * </pre>
 **/
public class Destination {
    private static Destination instance = null;
    private static PathAnalystHelper pathAnalystHelper;
    private static float[] offset = new float[]{ 213.84925f, 127.8224f};
    private static Workspace workspace;

    public Destination() {
    }

    public static Destination getInstance(){
        if (instance == null){
            synchronized (Destination.class){
                if (instance == null){
                    instance = new Destination();
                }
            }
        }
        return instance;
    }

    static byte[] pathAnalyst(float startX,float startY,float endX,float endY){
        //添加偏移量：
        startX += offset[0];
        startY += offset[1];
        endX += offset[0];
        endY += offset[1];

        try {
            if (pathAnalystHelper == null){
                //传入指定数据源
                pathAnalystHelper = new PathAnalystHelper(workspace.getDatasources().get(0));
            }
        }catch (Exception e){
            Log.e("IKKYU_Destination", "pathAnalyst: ", e);
        }

        //根据起点、终点坐标生成路径
        List<Point3D> pathLocationPs = createPath(startX, startY, endX, endY);
        if (pathLocationPs == null)return null;

//        List<Point3D> pathLocationPs = Arrays.asList(new Point3D(0,0,0),new Point3D(0,13.6F,0),new Point3D(-7.2f,13.6f,0));

        //坐标转换：地理坐标=>AR坐标=>Unity坐标
        Vector3[] result = getVector3s(pathLocationPs);
        String s = new Gson().toJson(result);
        return s.getBytes(StandardCharsets.UTF_8);
    }
    public static String doPathAnalyst(float startX,float startY,float endX,float endY){
        //添加偏移量：
        startX += offset[0];
        startY += offset[1];
        endX += offset[0];
        endY += offset[1];

        try {
            if (pathAnalystHelper == null){
                //传入指定数据源
                pathAnalystHelper = new PathAnalystHelper(workspace.getDatasources().get(0));
            }
        }catch (Exception e){
            Log.e("IKKYU_Destination", "pathAnalyst: ", e);
        }

        //根据起点、终点坐标生成路径
        List<Point3D> pathLocationPs = createPath(startX, startY, endX, endY);
        if (pathLocationPs == null)return null;

//        List<Point3D> pathLocationPs = Arrays.asList(new Point3D(0,0,0),new Point3D(0,13.6F,0),new Point3D(-7.2f,13.6f,0));

        //坐标转换：地理坐标=>AR坐标=>Unity坐标
        Vector3[] result = getVector3s(pathLocationPs);
        String s = new Gson().toJson(result);
        return s;
    }

    private static Vector3[] getVector3s(List<Point3D> pathLocationPs) {
        List<Point3D> flowElementNodes = new ArrayList<>();
//        flowElementNodes.addAll(pathLocationPs);
        //计算结果减去偏移量
        for (int i = 0; i < pathLocationPs.size(); i++) {
            Point3D p = pathLocationPs.get(i);
            flowElementNodes.add(new Point3D(p.x - offset[0], p.y - offset[1],p.z));
        }

        //过滤相同点
        flowElementNodes = PointsFilter.getNonRepeatPoints(flowElementNodes);

        //根据距离间隔分割点，生成新的点集
        flowElementNodes = PresetUtils.genNewPointsBySpacingDistance(flowElementNodes, 1.6f);
        //过滤相同点
        flowElementNodes = PointsFilter.getNonRepeatPoints(flowElementNodes);

        Vector3[] result = new Vector3[flowElementNodes.size()];
        for (int i = 0; i < flowElementNodes.size(); i++) {
            Point3D point3D = flowElementNodes.get(i);
            point3D.z = -1.6f;
            //转为unity中的左手坐标系
//            result[i] = CoordinateUtils.toLeftCoordinate(point3D);
            result[i] = CoordinateUtils.convert(PointConvertTool.convertToVector3(point3D));
        }
        return result;
    }

    //<editor-fold>本地静态方法
    @Nullable
    private static List<Point3D> createPath(double startX, double startY, double endX, double endY) {
        Point2Ds point2Ds = new Point2Ds();
        point2Ds.add(new Point2D(startX, startY));
        point2Ds.add(new Point2D(endX, endY));
        TransportationAnalystResult naviResult = pathAnalystHelper.getPath(/*自定义字段名称*/"glasses_Network", point2Ds);
        GeoLineM[] navi = naviResult.getRoutes();

        Point2D item;

        //解析naviPath
        List<Point3D> pathLocationPs = new ArrayList<>();
        for (int i = 0; i < navi.length; i++) {
            Point2Ds part = navi[i].convertToLine().getPart(i);

            for (int j = 0; j < part.getCount(); j++) {
                item = part.getItem(j);
                //记录原始路径数据
                pathLocationPs.add(new Point3D((float) item.getX(), (float)item.getY(), 0));
//                EqLog.i("\nPoint3D:",point3D.toString());
            }
        }

        pathLocationPs.add(new Point3D((float)endX, (float)endY,0));

        //过滤相同地理坐标
        pathLocationPs = PointsFilter.getNonRepeatLocation(pathLocationPs);
        return pathLocationPs;
    }

    //</editor-fold>


    public void init(Workspace workspace) {
        this.workspace = workspace;
    }
}
