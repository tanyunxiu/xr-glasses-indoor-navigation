package com.supermap.ar.path;

import com.supermap.ar.Point3D;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * 点过滤器
 */
public class PointsFilter {

    public static Point3D getNearPoint3D_this(Point3D o,Point3D a, Point3D b){
        if(a.x == b.x && a.y==b.y && a.z == b.z){
            return new Point3D(a.x,a.y,a.z);
        }

        float k1 = (a.x - o.x) * (b.x - a.x) + (a.y -o.y) * (b.y - a.y) + (a.z - o.z) * (b.z - a.z);
        float k2 = (float) (Math.pow(b.x - a.x,2) + Math.pow(b.y - a.y,2) + Math.pow(b.z - a.z,2));
        float k = -k1 / k2;

        o.x = k * (b.x - a.x) + a.x;
        o.y = k * (b.y - a.y) + a.y;
        o.z = k * (b.z - a.z) + a.z;
        return o;
    }

    /**
     * 过滤前后相同的点
     * @param list
     * @return
     */
    public static List<Point3D> getNonRepeatPoints(List<Point3D> list){
        int size = list.size();
        if (size < 3){
            return list;
        }
        List<Point3D> resultList = new ArrayList<>();
        Point3D currentPoint;
        Point3D nextPoint;
        for (int i = 0; i < size - 1; i++) {
            currentPoint = list.get(i);
            nextPoint = list.get(i + 1);

            if (Math.abs(nextPoint.x - currentPoint.x) < 0.5f
                    && Math.abs(nextPoint.y - currentPoint.y) < 0.5f
                    && Math.abs(nextPoint.z - currentPoint.z) < 0.5f){
                //..滤掉的内容
            }else {
                resultList.add(currentPoint);
            }
        }

        return resultList;
    }

    public static List<Point3D> getNonRepeatLocation(List<Point3D> list) {
        int size = list.size();
        if (size < 3){
            return null;
        }
        List<Point3D> resultList = new ArrayList<>();
        Point3D currentPoint;
        Point3D nextPoint;
        for (int i = 0; i < size - 1; i++) {
            currentPoint = list.get(i);
            nextPoint = list.get(i + 1);

            if (nextPoint.z == currentPoint.z
                && nextPoint.x == currentPoint.x
                && nextPoint.y == currentPoint.y){
                //..滤掉的内容
            }else {
                resultList.add(currentPoint);
            }
        }

        return resultList;
    }
}
