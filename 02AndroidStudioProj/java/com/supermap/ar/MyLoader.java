package com.supermap.ar;

import android.util.Log;

import com.supermap.ar.unity.ILoader;
import com.supermap.ar.unity.slam.SlamObject;

/**
 * @author tanyx 2022/3/23
 * @version 1.0
 * <br/>SampleCode:<br/>
 * <code>
 *
 * </code>
 **/
public class MyLoader implements ILoader {
    public static final String TAG = "IKKYU_"+MyLoader.class.getSimpleName();



    @Override
    public void start() {
        Log.i(TAG, "initILoader: "+ SlamObject.getInstance().getConfig());

    }

    @Override
    public void update() {
        Log.i("IKK2YU", "updateILoader: ");
    }
    @Override
    public void destroy() {
        Log.i(TAG, "destroy: ");
    }
}
