package com.unity3d.player;

import android.util.Log;

import com.supermap.ar.MyLoader;
import com.supermap.ar.unity.ILoaderManager;

/**
 * @author tanyx 2022/3/23
 * @version 1.0
 * <br/>SampleCode:<br/>
 * <code>
 *
 * </code>
 **/
public class Main {
    public static void main(String[] args) {
        Log.i("IKKYU_MAIN", "main: ");
        ILoaderManager.getInstance().add(new MyLoader());
    }
}
